#!/usr/bin/env python3

import os
import random
import sys

from osgeo import gdal, ogr

quads_pool = [] # list of (id, prio, distance from center)
volunteers_pool = [] # list of (name, nb of quads to assign)
affectations = {}
seed = random.seed(19711202) # Change this to a specific number to be repeatable

if len(sys.argv) != 4:
    print('''sortinghat.py <input_volunteerfile> <input_vectorfile> <output_vectorfile>''')
    exit(1)

out_ext = os.path.splitext(sys.argv[3])[1]
if out_ext == '.json':
    drv = ogr.GetDriverByName('GeoJSON')
elif out_ext == '.shp':
    drv = ogr.GetDriverByName('ESRI Shapefile')
elif out_ext == '.sqlite':
    drv = ogr.GetDriverByName('SQLite')
else:
    print('''extention not handled yet''')
    exit(1)

# Load the volunteers (mainly copy-paste from slack)
for line in open(sys.argv[1], 'r').readlines():
    v_clean = line.strip(' @\n')
    volunteers_pool.append((v_clean, 1))
    affectations[v_clean] = []

# Load the quads_pool from vector file, create a output copy
in_ds = gdal.OpenEx(sys.argv[2], gdal.OF_VECTOR|gdal.OF_VERBOSE_ERROR) or exit(1)
if os.path.exists(sys.argv[3]):
    drv.DeleteDataSource(sys.argv[3])
out_ds = drv.CreateDataSource(sys.argv[3])
# Get input layer & properties
in_layer = in_ds.GetLayer(0)
in_layer_defn = in_layer.GetLayerDefn()
extent = in_layer.GetExtent()
center = ogr.Geometry(ogr.wkbPoint)
center.AddPoint(extent[0]+(extent[1]-extent[0])/2,
                extent[2]+(extent[3]-extent[2])/2)
# Set output layer & properties
out_layer = out_ds.CreateLayer(os.path.splitext(os.path.basename(sys.argv[3]))[0],
                               in_layer.GetSpatialRef(),
                               ogr.wkbPolygon)
for i in range(in_layer_defn.GetFieldCount()):
    out_layer.CreateField(in_layer_defn.GetFieldDefn(i))
for feature in in_layer:
    mapper_name = feature.GetFieldAsString('map_name')
    if mapper_name:
        # Quad already assigned, write it in output
        mapper_name_clean = ' '.join(mapper_name.splitlines()[0].split(',')[-1::-1])
        if (mapper_name_clean, 1) in volunteers_pool:
            volunteers_pool.remove((mapper_name_clean, 1))
        affectations[mapper_name_clean].append(feature.GetFieldAsString('PageName'))
        feature.SetField('map_name', mapper_name_clean)
        out_layer.CreateFeature(feature)
    else:
        # Quad to be assigned
        fid = feature.GetFID()
        fprio = feature.GetFieldAsInteger('Priorety')
        fgeom = feature.GetGeometryRef()
        fdst = center.Distance(fgeom.Centroid())
        quads_pool.append((fid, fprio, fdst))
quads_pool.sort(key=lambda tup: (1.000001-tup[1])*tup[2])

# Let's go!
while len(quads_pool) > 0:
    if len(volunteers_pool) == 0:
        break

    # Next quad
    q, qdst, qprio = quads_pool.pop(0)
    # Get a volunteer
    v, vn = volunteers_pool.pop(random.randrange(len(volunteers_pool)))

    if q in affectations[v]:
        # If volunteer already selected for this quad, try again
        continue
    else:
        # Give this quad to this volunteer
        feature = in_layer.GetFeature(q)
        affectations[v].append(feature.GetFieldAsString('PageName'))
        feature.SetField('map_name', v)
        out_layer.CreateFeature(feature)
        # Update volunteers_pool
        if vn > 1:
            volunteers_pool.insert(0, (v, vn-1))

# Close datasets, print results
for v in sorted(affectations):
    print(' -', v, ':', ', '.join(affectations[v]))
if len(quads_pool) > 0:
    print('%d remaining in quads_pool' % len(quads_pool))
    for q, _, _ in quads_pool:
        feature = in_layer.GetFeature(q)
        out_layer.CreateFeature(feature)
if len(volunteers_pool) > 0:
    print('%d remaining in volunteeers_pool' % len(volunteers_pool))
del in_ds, out_ds
