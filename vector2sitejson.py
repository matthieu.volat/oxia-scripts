#!/usr/bin/env python3

import json
import os
import sys

from osgeo import gdal, ogr, osr

out = {
    "sites": [
        { "name": "Oxia Planum",
          "code": "OP",
          "view": [ "18.15", "-24.30", "11" ] },
        { "name": "Pannonia",
          "code": "PAN",
          "view": [ "18.32", "-24.43", "12" ] },
        { "name": "Dalmatia",
          "code": "DAL",
          "view": [ "18.17", "-24.34", "12" ] },
        { "name": "Aquitania",
          "code": "AQU",
          "view": [ "18.04", "-24.21", "12" ] }
    ]
}

# Open input dataset
in_ds = gdal.OpenEx(sys.argv[1], gdal.OF_VECTOR|gdal.OF_VERBOSE_ERROR) or exit(1)
in_layer = in_ds.GetLayer(0)
in_srs = in_layer.GetSpatialRef()

# Create a transform between input and target SRS
out_srs =  osr.SpatialReference()
out_srs.ImportFromWkt('''GEOGCRS["unnamed ellipse",DATUM["D_unknown",ELLIPSOID["Unknown",3396190,169.894447223611,LENGTHUNIT["metre",1,ID["EPSG",9001]]]],PRIMEM["Greenwich",0,ANGLEUNIT["Degree",0.0174532925199433]],CS[ellipsoidal,2],AXIS["longitude",east,ORDER[1],ANGLEUNIT["Degree",0.0174532925199433]],AXIS["latitude",north,ORDER[2],ANGLEUNIT["Degree",0.0174532925199433]]]''')
transform = osr.CoordinateTransformation(in_srs, out_srs)

# Proceed
for feature in in_layer:
    name = feature.GetFieldAsString('Assignment')
    geom = feature.GetGeometryRef()
    center = geom.Centroid()
    center.Transform(transform)
    out['sites'].append({ 'name': name,
                          'code': name,
                          'view': [ "%.02f"%center.GetY(), "%.02f"%center.GetX(), "16" ] })

# Print results
print(json.dumps(out, indent=4, sort_keys=False))
